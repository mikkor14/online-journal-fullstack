using Microsoft.AspNetCore.Mvc;
using react_auth.Data;
using react_auth.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace react_auth.Controllers
{

    [ApiController]
    [Route("/api/entries")]
    public class JournalTaskController : ControllerBase
    {

        private readonly ApplicationDbContext context;

        public JournalTaskController(ApplicationDbContext context)
        {
            this.context = context;
        }

        //Get entries
        [HttpGet]
        public ActionResult<IEnumerable<JournalEntry>> GetAll()
        {
            return context.JournalEntries;
        }

        //Post entry
        [HttpPost]
        public ActionResult<JournalEntry> AddEntry(JournalEntry entry)
        {
            entry.dateCreated = DateTime.Now;
            entry.dateEdited = DateTime.Now;
            context.JournalEntries.Add(entry);
            context.SaveChanges();

            return CreatedAtAction("GetEntryFromId", new JournalEntry { Id = entry.Id }, entry);
        }

        //Delete entry
        [HttpDelete("{id}")]
        public ActionResult<JournalEntry> DeleteEntry(int id)
        {
            var entry = context.JournalEntries.Find(id);

            if (entry == null)
            {
                return NotFound();
            }

            context.JournalEntries.Remove(entry);
            context.SaveChanges();

            return entry;
        }

        //Edit entry
        [HttpPut("{id}")]
        public ActionResult UpdateEntry(int id, JournalEntry entry)
        {
            if (id != entry.Id)
            {
                return BadRequest();
            }

            //extra feedback
            entry.dateEdited = DateTime.Now;
            context.Entry(entry).State = EntityState.Modified;
            context.SaveChanges();

            return NoContent();
        }

    }
}
