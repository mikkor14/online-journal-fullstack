﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace react_auth.Data.Migrations
{
    public partial class seedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO JournalEntries(Title,Subtitle,Text,dateCreated,dateEdited) VALUES('TestTitle','TestSubtitle','This is some random text',GETDATE(),GETDATE())");
            migrationBuilder.Sql("INSERT INTO JournalEntries(Title,Subtitle,Text,dateCreated,dateEdited) VALUES('TestTitle2','TestSubtitle','This is some random text',GETDATE(),GETDATE())");
            migrationBuilder.Sql("INSERT INTO JournalEntries(Title,Subtitle,Text,dateCreated,dateEdited) VALUES('TestTitle3','TestSubtitle','This is some random text',GETDATE(),GETDATE())");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM JournalEntries WHERE Title='TestTitle'");
            migrationBuilder.Sql("DELETE FROM JournalEntries WHERE Title='TestTitle2'");
            migrationBuilder.Sql("DELETE FROM JournalEntries WHERE Title='TestTitle3'");

        }
    }
}
