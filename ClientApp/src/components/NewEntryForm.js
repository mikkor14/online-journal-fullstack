import React, { Component } from "react";

export class NewEntryForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			title: "",
			subtitle: "",
			text: ""
		};

		this.handleChangeTitle = this.handleChangeTitle.bind(this);
		this.handleChangeSubtitle = this.handleChangeSubtitle.bind(this);
		this.handleChangeText = this.handleChangeText.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	//handle change for events
	handleChangeTitle(event) {
		this.setState({ title: event.target.value });
	}

	handleChangeSubtitle(event) {
		this.setState({ subtitle: event.target.value });
	}

	handleChangeText(event) {
		this.setState({ text: event.target.value });
	}

	handleSubmit(event) {
		this.props.history.push("/");
		event.preventDefault();
		this.sendData();
		this.setState({
			title: "",
			subtitle: "",
			text: ""
		});

		setTimeout(
			function() {
				this.props.history.push("/EntryCollection");
			}.bind(this),
			1000
		);
	}

	sendData = () => {
		fetch("http://localhost:38254/api/entries", {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				title: this.state.title,
				subTitle: this.state.subtitle,
				text: this.state.text
			})
		})
			.then(r => {
				console.log(r);
			})
			.then(error => console.log(error));
	};

	render() {
		return (
			<div className="col-sm-12 card-color mt-4">
				<div className="card card-body">
					<form onSubmit={this.handleSubmit}>
						<h3 className="card-title">
							<a> Add a new entry</a>
						</h3>
						<title>Add a new journal entry</title>
						<div className="form-group">
							<label>Add a title:</label> <br />
							<input
								textarea
								className="form-control"
								id="new-card-title"
								type="text"
								name="title"
								value={this.state.title}
								onChange={this.handleChangeTitle}
							/>
						</div>
						<div className="form-group">
							<label>Add a subtitle:</label> <br />
							<input
								textarea
								className="form-control"
								type="text"
								name="subtitle"
								value={this.state.subtitle}
								onChange={this.handleChangeSubtitle}
							/>
						</div>
						<div className="form-group">
							<label>Add text:</label>
							<br />
							<textarea
								className="form-control"
								type="text"
								name="text"
								value={this.state.text}
								onChange={this.handleChangeText}
							/>
						</div>
						<div className="form-group">
							<button
								type="submit"
								className="btn btn-primary"
								onClick={this.handleSubmit}
								value="Submit"
							>
								Add entry
							</button>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default NewEntryForm;
