import React, { Component } from "react";
import Modal from "./Modal";
import Moment from "react-moment";

class JournalEntry extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modalOpen: false,
			id: props.id,
			title: props.title,
			subtitle: props.subtitle,
			text: props.text,
			dateCreated: props.dateCreated,
			dateEdited: props.dateEdited
		};
		this.setModalState = this.setModalState.bind(this);
	}

	setModalState() {
		this.setState({
			modalOpen: !this.state.modalOpen
		});
	}

	deleteData = entryId => {
		const requestOptions = {
			method: "DELETE"
		};
		fetch(`http://localhost:38254/api/entries/${entryId}`, requestOptions)
			.then(response => {
				return response.json();
			})
			.then(result => {
				console.log("Successfully deleted entry");
			});
		this.props.refresh();
	};

	render() {
		return (
			<div id={`entry-${this.state.id}`} className="col-sm-12 card-color mt-4">
				<div className="card card-content">
					<div className="card card-body">
						<h5 type="text" className="card-title" id="new-card-title">
							{this.state.title}
						</h5>
						<h6 className="card-subtitle" id="new-card-subtitle">
							{this.state.subtitle}
						</h6>
						<p className="card-text" id="new-card-text">
							{this.state.text}
						</p>
						<p className="card-text" id="new-card-dateCreated">
							Created: <Moment parse="YYYY-MM-DD HH:mm">{this.state.dateCreated}</Moment>
						</p>
						<p className="card-text" id="new-card-dateEdited">
							Last edited: <Moment>{this.state.dateEdited}</Moment>
						</p>
						<div className="form-group">
							<button
								type="submit"
								id="myBtn"
								className="btn btn-primary btn-md"
								onClick={this.setModalState}
							>
								Edit
							</button>
							<button
								type="submit"
								className="btn btn-danger btn-md"
								onClick={() => {
									this.deleteData(this.props.id);
								}}
							>
								Delete
							</button>
						</div>
						<Modal
							id={this.state.id}
							modalOpen={this.state.modalOpen}
							title={this.state.title}
							subtitle={this.state.subtitle}
							text={this.state.text}
							dateCreated={this.state.dateCreated}
							setModalState={this.setModalState}
							refresh={this.props.refresh}
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default JournalEntry;
