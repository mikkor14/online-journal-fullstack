import React, { Component } from "react";
import JournalEntry from "./JournalEntry";

class EntryCollection extends Component {
	constructor(props) {
		super(props);

		this.state = {
			entries: []
		};
	}

	componentDidMount() {
		this.getData();
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
	}


	getData = () => {
		fetch(`http://localhost:38254/api/entries`)
			.then(resp => resp.json())
			.then(data => {
				console.log(data);
				this.setState({
					entries: data
				});
			})
			.catch(error => {
				console.error(error);
			});
	};

	refresh = () => {

		//Timeout to let the database catch up to the frontend updating
		setTimeout(
			function() {
				this.getData();
			}
				.bind(this),
			1000
		);
	};


	render() {
		let entries = null;

		if (this.state.entries.length > 0) {
			entries = this.state.entries.map(entry => (
				<JournalEntry
					key={entry.id}
					id={entry.id}
					title={entry.title}
					subtitle={entry.subTitle}
					text={entry.text}
					dateCreated={entry.dateCreated}
					dateEdited={entry.dateEdited}
					refresh={this.refresh}
				/>
			));
		}

		return (
			<div>
				{entries}
			</div>
		);
	}
}

export default EntryCollection;