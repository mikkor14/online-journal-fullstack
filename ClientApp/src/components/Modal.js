import React, { Component } from "react";
import { default as ReactModal } from "react-modal";

class Modal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id: this.props.id,
			title: this.props.title,
			subtitle: this.props.subtitle,
			text: this.props.text,
			dateCreated: this.props.dateCreated
		};
		this.handleCloseModal = this.handleCloseModal.bind(this);
		this.handleChangeTitle = this.handleChangeTitle.bind(this);
		this.handleChangeSubtitle = this.handleChangeSubtitle.bind(this);
		this.handleChangeText = this.handleChangeText.bind(this);
	}

	handleCloseModal() {
		this.setState({ isOpen: false });
	}

	//handle change for events
	handleChangeTitle(event) {
		this.setState({ title: event.target.value });
	}

	handleChangeSubtitle(event) {
		this.setState({ subtitle: event.target.value });
	}

	handleChangeText(event) {
		this.setState({ text: event.target.value });
	}

	editData = () => {
		const url = `http://localhost:38254/api/entries/${this.state.id}`;
		const data = {
			id: this.state.id,
			title: this.state.title,
			subtitle: this.state.subtitle,
			text: this.state.text,
			dateCreated: this.props.dateCreated
		};

		fetch(url, {
			method: "PUT",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			},
			body: JSON.stringify(data)
		})
			.then(r => {
				console.log(r);
			})
			.then(error => console.log(error));

		this.props.refresh();

	};

	render() {
		return (
			<div>
				<ReactModal
					isOpen={this.props.modalOpen}
					onRequestClose={this.handleCloseModal}
					id="react-modal"
				>
					<div className="card card-body">
						<h4 className="card-title">
							<a> Edit the entry</a>
						</h4>
						<div className="form-group">
							<h1>
								<input
									type="text"
									className="form-control"
									id="new-card-title"
									defaultValue={this.props.title}
									onChange={this.handleChangeTitle}
								/>
							</h1>
						</div>
						<div className="form-group">
							<h6>
								<input
									type="text"
									className="form-control"
									id="new-card-subtitle"
									defaultValue={this.props.subtitle}
									onChange={this.handleChangeSubtitle}
								/>
							</h6>
						</div>
						<div className="form-group">
							<textarea
								type="text"
								className="form-control"
								id="new-card-text"
								defaultValue={this.props.text}
								onChange={this.handleChangeText}
							/>
						</div>
						<div className="form-group">
							<button
								className="btn btn-success btn-md"
								id="myBtn"
								onClick={() => {
									this.editData(this.props.id);
								}}
							>
								Save
							</button>
							<button className="btn btn-secondary btn-md" onClick={this.props.setModalState}>
								Close Modal
							</button>
						</div>
					</div>
				</ReactModal>
			</div>
		);
	}
}

export default Modal;
