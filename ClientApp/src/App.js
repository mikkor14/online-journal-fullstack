import React, {Component} from 'react';
import {Route} from 'react-router';
import {Layout} from './components/Layout';
import ApiAuthorizationRoutes from './components/api-authorization/ApiAuthorizationRoutes';
import {ApplicationPaths} from './components/api-authorization/ApiAuthorizationConstants';

import './custom.css'
import EntryCollection from "./components/EntryCollection";
import NewEntryForm from "./components/NewEntryForm";

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route path="/" component={NewEntryForm}/>
                <Route path="/EntryCollection" component={EntryCollection}/>
                <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes}/>
            </Layout>
        );
    }
}
