﻿using System;

namespace react_auth.Models
{
    public class JournalEntry
    {
        public int Id { get; set; }
        public String Title { get; set; }
        public String SubTitle { get; set; }
        public String Text { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime dateEdited { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }

}
